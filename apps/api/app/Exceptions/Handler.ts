import Logger from '@ioc:Adonis/Core/Logger'
import HttpExceptionHandler from '@ioc:Adonis/Core/HttpExceptionHandler'
import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";

export default class ExceptionHandler extends HttpExceptionHandler {
  constructor() {
    super(Logger)
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public override async handle(error: any, context: HttpContextContract) {
    if (error.status === 422)
      return context.response.status(422).send({
        errors: {
          body: error.messages.errors
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            ? error.messages.errors.map((error: any) => error.field + ': ' + error.message)
            : error.message,
        },
      })

    return super.handle(error, context)
  }
}
