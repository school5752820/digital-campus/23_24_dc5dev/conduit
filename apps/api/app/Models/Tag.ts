import { BaseModel, column, manyToMany, ManyToMany } from '@ioc:Adonis/Lucid/Orm';
import { DateTime } from 'luxon';
import Article from 'App/Models/Article.ts';

export default class Tag extends BaseModel {
  @column({ isPrimary: true })
	public declare name: string;

	@manyToMany(() => Article, {
		pivotTable: 'articles_tags',
	})
	public declare articles: ManyToMany<typeof Article>;

	@column.dateTime({ autoCreate: true })
	public declare createdAt: DateTime;
}
