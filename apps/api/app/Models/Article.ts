import {
	BaseModel,
	BelongsTo,
	belongsTo,
	column,
	HasMany,
	hasMany,
	manyToMany,
	ManyToMany,
} from '@ioc:Adonis/Lucid/Orm';
import { slugify } from '@ioc:Adonis/Addons/LucidSlugify';
import { DateTime } from 'luxon';
import Tag from 'App/Models/Tag.ts';
import User from 'App/Models/User.ts';
import Comment from 'App/Models/Comment.ts';

export default class Article extends BaseModel {
	@column({ isPrimary: true, serializeAs: null })
	public declare id: number;

	@column()
	public declare title: string;

	@column()
	@slugify({
		strategy: 'simple',
		fields: ['title'],
		allowUpdates: true,
	})
	public declare slug: string;

	@column()
	public declare description: string;

	@column()
	public declare body: string;

	@manyToMany(() => Tag, {
		pivotTable: 'articles_tags',
		relatedKey: 'name',
	})
	public declare tagList: ManyToMany<typeof Tag>;

	@column({ columnName: 'author', serializeAs: 'author' })
	public declare authorId: number;

	@belongsTo(() => User, {
		foreignKey: 'authorId',
	})
	public declare author: BelongsTo<typeof User>;

	@manyToMany(() => User, {
		pivotTable: 'favorites',
	})
	public declare favorites: ManyToMany<typeof User>;

	@hasMany(() => Comment)
	public declare comments: HasMany<typeof Comment>;

	@column.dateTime({ autoCreate: true, serializeAs: 'createdAt' })
	public declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true, serializeAs: 'updatedAt' })
	public declare updatedAt: DateTime;
}
