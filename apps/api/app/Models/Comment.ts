import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm';
import { DateTime } from 'luxon';
import Article from 'App/Models/Article.ts';
import User from 'App/Models/User.ts';

export default class Comment extends BaseModel {
	@column({ isPrimary: true })
	public declare id: number;

	@column()
	public declare body: string;

	@column({ columnName: 'author', serializeAs: 'authorId' })
	public declare authorId: number;

	@belongsTo(() => User, {
		foreignKey: 'authorId',
	})
	public declare author: BelongsTo<typeof User>;

	@column({ columnName: 'article', serializeAs: 'articleId' })
	public declare articleId: number;

	@belongsTo(() => Article, {
		foreignKey: 'articleId',
	})
	public declare article: BelongsTo<typeof Article>;

	@column.dateTime({ autoCreate: true, serializeAs: 'createdAt' })
	public declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true, serializeAs: 'updatedAt' })
	public declare updatedAt: DateTime;
}
