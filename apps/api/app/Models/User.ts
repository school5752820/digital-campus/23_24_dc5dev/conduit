import { BaseModel, column, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm';
import Hash from '@ioc:Adonis/Core/Hash';
import Article from 'App/Models/Article.ts';
import { DateTime } from 'luxon';
import { beforeSave } from '@adonisjs/lucid/build/src/Orm/Decorators';

export default class User extends BaseModel {
	@column({ isPrimary: true })
	public declare id: number;

	@column()
	public declare username: string;

	@column()
	public declare email: string;

	@column({ serializeAs: null })
	public declare password: string;

	@column()
	public declare bio: string | null;

	@column()
	public declare image: string | null;

	@column({ serializeAs: null })
	public declare rememberMeToken?: string;

	@manyToMany(() => User, {
		serializeAs: null,
		pivotTable: 'follows',
		pivotForeignKey: 'follower',
		pivotRelatedForeignKey: 'following',
	})
	public declare followings: ManyToMany<typeof User>;

	@manyToMany(() => User, {
		serializeAs: null,
		pivotTable: 'follows',
		pivotForeignKey: 'following',
		pivotRelatedForeignKey: 'follower',
	})
	public declare followers: ManyToMany<typeof User>;

	@manyToMany(() => Article, {
		pivotTable: 'favorites',
	})
	public declare favorites: ManyToMany<typeof Article>;

	@column.dateTime({ autoCreate: true, serializeAs: null })
	public declare createdAt: DateTime;

	@column.dateTime({ autoCreate: true, autoUpdate: true, serializeAs: null })
	public declare updatedAt: DateTime;

	@beforeSave()
	public static async hashPassword(user: User) {
		if (user.$dirty.password) {
			user.password = await Hash.make(user.password);
		}
	}
}
