import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import User from 'App/Models/User';
import CreateUserValidator from 'App/Validators/CreateUserValidator';
import LoginValidator from 'App/Validators/LoginValidator';
import UpdateUserValidator from 'App/Validators/UpdateUserValidator';

export default class UsersController {
	public async login({ request, response, auth }: HttpContextContract) {
		const { user } = await request.validate(LoginValidator);
		await auth.use('web').attempt(user.email, user.password);

    if (undefined === auth.user) {
      return response.unprocessableEntity();
    }

		return response.ok(this.getUser(auth.user));
	}

  public async logout({ auth, response }: HttpContextContract) {
    if (undefined === auth.user) {
      return response.unauthorized();
    }

    await auth.use('web').logout();

    return response.noContent();
  }

	public async me({ response, auth }: HttpContextContract) {
    if (undefined === auth.user) {
      return response.unauthorized();
    }

		return response.ok(this.getUser(auth.user));
	}

	public async store({ request, response, auth }: HttpContextContract) {
		const { user: userPayload } = await request.validate(CreateUserValidator);

		await User.create(userPayload);
		await auth.use('web').attempt(userPayload.email, userPayload.password);

    if (undefined === auth.user) {
      return response.unprocessableEntity();
    }

		return response.created(this.getUser(auth.user));
	}

	public async update({ request, response, auth }: HttpContextContract) {
    if (undefined === auth.user) {
      return response.unauthorized();
    }

		const { user: userPayload } = await request.validate(UpdateUserValidator);

    auth.user.merge(userPayload);
		await auth.user.save();

		return response.ok(this.getUser(auth.user));
	}

	private getUser(user: User) {
    return {
			user: {
				email: user.email,
				username: user.username,
				bio: user.bio,
				image: user.image,
			},
		};
	}
}
