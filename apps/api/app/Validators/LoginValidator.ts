import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import { rules, schema } from '@ioc:Adonis/Core/Validator';

export default class LoginValidator {
	constructor(protected context: HttpContextContract) {}

	public schema = schema.create({
		user: schema.object().members({
			email: schema.string({}, [rules.email()]),
			password: schema.string({}, [rules.minLength(6)]),
		}),
	});

	public messages = {};
}
