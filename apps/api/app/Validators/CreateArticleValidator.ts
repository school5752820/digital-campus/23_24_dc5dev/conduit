import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import { rules, schema } from '@ioc:Adonis/Core/Validator';

export default class CreateArticleValidator {
	constructor(protected context: HttpContextContract) {}

	public schema = schema.create({
		article: schema.object().members({
			title: schema.string({}, [rules.minLength(3)]),
			description: schema.string(),
			body: schema.string(),
			tagList: schema.array.optional().members(schema.string()),
		}),
	});

	public messages = {};
}
