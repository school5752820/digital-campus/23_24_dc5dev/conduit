import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import { rules, schema } from '@ioc:Adonis/Core/Validator';

export default class UpdateArticleValidator {
	constructor(protected context: HttpContextContract) {}

	public schema = schema.create({
		article: schema.object().members({
			title: schema.string.optional({}, [rules.minLength(3)]),
			description: schema.string.optional(),
			body: schema.string.optional(),
		}),
	});

	public messages = {};
}
