import { schema, rules } from '@ioc:Adonis/Core/Validator';
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';

export default class CreateUserValidator {
	constructor(protected context: HttpContextContract) {}

	public schema = schema.create({
		user: schema.object().members({
			username: schema.string({}, [rules.unique({ table: 'users', column: 'username' })]),
			email: schema.string({}, [rules.email(), rules.unique({ table: 'users', column: 'email' })]),
			password: schema.string({}, [rules.minLength(6)]),
			bio: schema.string.optional(),
			image: schema.string.optional(),
		}),
	});

	public messages = {};
}
