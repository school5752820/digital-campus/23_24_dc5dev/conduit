import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import { rules, schema } from '@ioc:Adonis/Core/Validator';

export default class UpdateUserValidator {
	constructor(protected context: HttpContextContract) {}

	public schema = schema.create({
		user: schema.object().members({
			username: schema.string.optional({}),
			email: schema.string.optional({}, [rules.email()]),
			password: schema.string.optional({}, [rules.minLength(6)]),
			bio: schema.string.optional(),
			image: schema.string.optional(),
		}),
	});

	public messages = {};
}
