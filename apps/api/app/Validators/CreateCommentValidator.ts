import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import { schema } from '@ioc:Adonis/Core/Validator';

export default class CreateCommentValidator {
	constructor(protected context: HttpContextContract) {}

	public schema = schema.create({
		comment: schema.object().members({
			body: schema.string(),
		}),
	});

	public messages = {};
}
