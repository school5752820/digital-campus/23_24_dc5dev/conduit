import Bouncer from '@ioc:Adonis/Addons/Bouncer';
import Comment from 'App/Models/Comment.ts';
import User from 'App/Models/User.ts';
import Article from 'App/Models/Article.ts';

export const { actions } = Bouncer.define('updateArticle', (user: User, article: Article) => {
	return user.id === article.authorId;
})
	.define('removeArticle', (user: User, article: Article) => {
		return user.id === article.authorId;
	})
	.define('removeComment', (user: User, comment: Comment) => {
		return user.id === comment.authorId;
	});

export const { policies } = Bouncer.registerPolicies({});
