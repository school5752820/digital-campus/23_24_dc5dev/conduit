import Route from '@ioc:Adonis/Core/Route';
import HealthCheck from '@ioc:Adonis/Core/HealthCheck';

Route.get('/', async () => {
	return { hello: 'world' };
});

Route.get('/health', async ({ response }) => {
	const report = await HealthCheck.getReport();

	return report.healthy ? response.ok(report) : response.badRequest(report);
});

Route.group(() => {
	Route.get('/user', 'UsersController.me').middleware('auth');
	Route.post('/users/login', 'UsersController.login');
	Route.post('/users/logout', 'UsersController.logout').middleware('auth');
	Route.post('/users', 'UsersController.store');
	Route.put('/user', 'UsersController.update').middleware('auth');

	Route.get('/profiles/:username', 'ProfilesController.show');
	Route.post('/profiles/:username/follow', 'ProfilesController.follow').middleware('auth');
	Route.delete('/profiles/:username/follow', 'ProfilesController.unfollow').middleware('auth');

	Route.get('/articles/feed', 'ArticlesController.feed').middleware('auth');
	Route.get('/articles/:slug', 'ArticlesController.show');
	Route.get('/articles', 'ArticlesController.index');
	Route.post('/articles', 'ArticlesController.store').middleware('auth');
	Route.put('/articles/:slug', 'ArticlesController.update').middleware('auth');
	Route.delete('/articles/:slug', 'ArticlesController.destroy').middleware('auth');

	Route.post('/articles/:slug/favorite', 'FavoritesController.favorite').middleware('auth');
	Route.delete('/articles/:slug/favorite', 'FavoritesController.unfavorite').middleware('auth');

	Route.get('/articles/:slug/comments', 'CommentsController.index');
	Route.post('/articles/:slug/comments', 'CommentsController.store').middleware('auth');
	Route.delete('/articles/:slug/comments/:id', 'CommentsController.destroy').middleware('auth');

	Route.get('/tags', 'TagsController.index');
}).prefix('/api');
