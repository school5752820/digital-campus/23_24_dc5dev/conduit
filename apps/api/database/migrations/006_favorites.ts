import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class Favorites extends BaseSchema {
	protected tableName = 'favorites';

	public override async up() {
		this.schema.createTable(this.tableName, (table) => {
			table
				.integer('article_id')
				.unsigned()
				.references('id')
				.inTable('articles')
				.onDelete('CASCADE')
				.notNullable();
			table.integer('user_id').unsigned().references('users.id').notNullable();
			table.unique(['article_id', 'user_id']);
			table.timestamp('created_at', { useTz: true });
			table.timestamp('updated_at', { useTz: true });
		});
	}

	public override async down() {
		this.schema.dropTable(this.tableName);
	}
}
