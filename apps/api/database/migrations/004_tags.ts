import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class Tags extends BaseSchema {
	protected tableName = 'tags';

	public override async up() {
		this.schema.createTable(this.tableName, (table) => {
      table.string('name').primary().notNullable();
			table.timestamp('created_at', { useTz: true });
		});
	}

	public override async down() {
		this.schema.dropTable(this.tableName);
	}
}
