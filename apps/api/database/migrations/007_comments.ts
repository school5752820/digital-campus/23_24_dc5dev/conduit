import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class Comments extends BaseSchema {
	protected tableName = 'comments';

	public override async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.increments('id').primary();
			table.text('body').notNullable();
			table.integer('author').references('users.id').notNullable();
			table.integer('article').references('articles.id').notNullable();
			table.timestamp('created_at', { useTz: true });
			table.timestamp('updated_at', { useTz: true });
		});
	}

	public override async down() {
		this.schema.dropTable(this.tableName);
	}
}
