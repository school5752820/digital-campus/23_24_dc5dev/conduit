import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class Follows extends BaseSchema {
	protected tableName = 'follows';

	public override async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.integer('follower').unsigned().references('users.id').notNullable();
			table.integer('following').unsigned().references('users.id').notNullable();
			table.unique(['follower', 'following']);
			table.timestamp('created_at', { useTz: true });
		});
	}

	public override async down() {
		this.schema.dropTable(this.tableName);
	}
}
