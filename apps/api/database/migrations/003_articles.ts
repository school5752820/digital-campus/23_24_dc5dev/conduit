import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class Articles extends BaseSchema {
	protected tableName = 'articles';

	public override async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.increments('id').primary();
			table.string('title').notNullable();
			table.string('slug').notNullable();
			table.text('description').notNullable();
			table.text('body').notNullable();
			table.integer('author').references('users.id').notNullable();
			table.timestamp('created_at', { useTz: true });
			table.timestamp('updated_at', { useTz: true });
		});
	}

	public override async down() {
		this.schema.dropTable(this.tableName);
	}
}
