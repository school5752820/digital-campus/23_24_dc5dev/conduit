import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class ArticlesTags extends BaseSchema {
	protected tableName = 'articles_tags';

	public override async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.integer('article_id').unsigned().references('articles.id').notNullable();
			table.string('tag_name').unsigned().references('tags.name').notNullable();
			table.unique(['article_id', 'tag_name']);
			table.timestamp('created_at', { useTz: true });
			table.timestamp('updated_at', { useTz: true });
		});
	}

	public override async down() {
		this.schema.dropTable(this.tableName);
	}
}
