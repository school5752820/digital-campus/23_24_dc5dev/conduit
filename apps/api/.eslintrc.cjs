const baseConfig = require('@conduit/config/eslint');

/** @type {import('eslint').ESLint.ConfigData} */
module.exports = {
  ...baseConfig,
  extends: [
    'plugin:adonis/typescriptApp',
    ...baseConfig.extends,
  ],
  env: {
    node: true,
  },
  parserOptions: {
    ...baseConfig.parserOptions,
    tsconfigRootDir: __dirname,
    project: [
      '../../lib/config/tsconfig.eslint.json',
      './tsconfig.json',
    ],
  },
  rules: {
    'unicorn/no-null': 'off',
    'unicorn/filename-case': 'off',
    'n/no-missing-import': 'off',
    'import/no-unresolved': 'off',
    '@typescript-eslint/no-unsafe-call': 'off',
    '@typescript-eslint/no-floating-promises': 'off',
    '@typescript-eslint/no-unsafe-assignment': 'off',
    '@typescript-eslint/no-unsafe-member-access': 'off',
    'unicorn/text-encoding-identifier-case': 'warn',
    '@typescript-eslint/require-await': 'off',
    'sonarjs/no-duplicate-string': 'warn',
  }
}
