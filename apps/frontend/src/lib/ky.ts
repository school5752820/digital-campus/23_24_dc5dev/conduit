import type { Options as KyOptions } from 'ky';
import { parse, type BaseSchema } from 'valibot';
import ky from 'ky';
import { API_BASE_URL } from '@/config';

interface CommonProps<TInput> {
	inputSchema: BaseSchema<TInput>;
	endpoint: string;
	expectedStatuses: number[];
	payload?: TInput;
	method?: KyOptions['method'];
	options?: Omit<KyOptions, 'method' | 'json'>;
}

type RequestProps<TInput, TOutput> =
	| (CommonProps<TInput> & {
			noContent?: undefined | false;
			outputSchema: BaseSchema<TOutput>;
	  })
	| (CommonProps<TInput> & {
			noContent: true;
			outputSchema?: undefined;
	  });

export async function makeRequest<TInput, TOutput>(
	props: RequestProps<TInput, TOutput>,
	validate = true,
): Promise<TOutput> {
	const {
		endpoint,
		method,
		inputSchema,
		outputSchema,
		expectedStatuses,
		payload: _payload,
		options: _options,
		noContent,
	} = props;

	const validatedPayload = validate ? parse(inputSchema, _payload) : _payload;

	const options: KyOptions = {
		..._options,
		method: method ?? 'GET',
		credentials: 'include',
		throwHttpErrors: false,
	};

	if (validatedPayload) {
		options.json = validatedPayload;
	}

	const response = await ky(API_BASE_URL + endpoint, options);

	if (!expectedStatuses.includes(response.status)) {
		throw new Error(
			`HTTP status not expected. Expected: ${expectedStatuses.join(',')} / Received: ${
				response.status
			}`,
		);
	}

	if (noContent) {
		return undefined as TOutput;
	}

	const jsonResponse = await response.json();

	return validate ? parse(outputSchema, jsonResponse) : (jsonResponse as TOutput);
}
