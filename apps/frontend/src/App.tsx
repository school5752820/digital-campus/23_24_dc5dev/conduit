import { RootProviders } from './providers/root.tsx';

export const App = () => {
	return <RootProviders />;
};
