import { Outlet, RootRoute, Router } from '@tanstack/react-router';
import { definition as authRoutes } from '../features/auth/routes';

export const rootRoute = new RootRoute({
	component: () => {
		return <Outlet />;
	},
});

const routeTree = rootRoute.addChildren([authRoutes]);
export const router = new Router({ routeTree });

declare module '@tanstack/react-router' {
	interface Register {
		router: typeof router;
	}
}
