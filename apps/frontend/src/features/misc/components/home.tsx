import type { PropsWithChildren } from 'react';
import { Fragment, useCallback, useEffect, useState } from 'react';

type HomeProps = PropsWithChildren;

function reactToResize() {
	console.log('hop resize!');
}

export const Home = (props: HomeProps) => {
	const { children } = props;
	const [users, setUsers] = useState<string[]>([]);

	useEffect(() => {
		window.addEventListener('resize', reactToResize);

		return () => {
			window.removeEventListener('resize', reactToResize);
		};
	}, []);

	const addUser = useCallback(() => {
		setUsers((prev) => {
			return [...prev, 'coucou'];
		});
	}, []);

	return (
		<Fragment>
			<h1>Accueil</h1>
			<button type="button" onClick={addUser}>
				Ajouter
			</button>
			<ul>
				{users.map((user, index) => (
					<li key={index}>{user}</li>
				))}
			</ul>
			{children}
		</Fragment>
	);
};
