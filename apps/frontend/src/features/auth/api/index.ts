import { makeRequest } from '../../../lib/ky.ts';
import {
	object as vobject,
	string as vstring,
	email as vemail,
	url as vurl,
	nullable as vnullable,
} from 'valibot';

interface LoginOptions {
	email: string;
	password: string;
}

export function login({ email, password }: LoginOptions) {
	return makeRequest({
		method: 'post',
		endpoint: '/users/login',
		inputSchema: vobject({
			user: vobject({
				email: vstring([vemail()]),
				password: vstring(),
			}),
		}),
		outputSchema: vobject({
			user: vobject({
				email: vstring([vemail()]),
				username: vstring(),
				bio: vnullable(vstring()),
				image: vnullable(vstring([vurl()])),
			}),
		}),
		payload: {
			user: { email, password },
		},
		expectedStatuses: [200, 422],
	});
}
