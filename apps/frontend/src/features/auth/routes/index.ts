import { Route, lazyRouteComponent } from '@tanstack/react-router';
import { rootRoute } from '../../../routes/index';

const authRoutes = new Route({
	getParentRoute: () => rootRoute,
	id: 'authenticated_routes',
});

export const loginRoute = new Route({
	getParentRoute: () => authRoutes,
	path: '/login',
	component: lazyRouteComponent(() => import('./login'), 'Login'),
});

export const definition = authRoutes.addChildren([loginRoute]);
