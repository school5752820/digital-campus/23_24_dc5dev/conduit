import { useCallback, useRef, useState, type ChangeEvent } from 'react';
import { login } from '../api';

export const Login = () => {
	// const formRef = useRef<HTMLFormElement>(null);
	const [email, setEmail] = useState(null);
	const [password, setPassword] = useState(null);

	const handlePasswordChange = useCallback((event: ChangeEvent) => {
		setPassword(() => event.target.value);
	});

	const handleEmailChange = useCallback((event: ChangeEvent) => {
		setEmail(() => event.target.value);
	}, []);

	const authenticate = useCallback(
		async (event: SubmitEvent) => {
			event.preventDefault();
			event.stopPropagation();
			// const data = new FormData(formRef.current);

			console.log({ email, password });
			try {
				const response = await login({
					email,
					password,
				});
				console.log({ response });
			} catch (error) {
				console.error(error);
			}
		},
		[email, password],
	);

	return (
		<>
			<h1>Connecte toi stp</h1>
			<form className="space-y-5" onSubmit={authenticate}>
				<div className="space-y-1">
					<label htmlFor="email">Votre e-mail</label>
					<input onChange={handleEmailChange} type="email" name="email" id="email" />
				</div>
				<div className="space-y-1">
					<label htmlFor="password">Mot de passe</label>
					<input onChange={handlePasswordChange} type="password" name="password" id="password" />
				</div>
				<button
					type="submit"
					className="rounded bg-green-600 text-green-50 px-4 py-3 hover:bg-green-800"
				>
					Connexion
				</button>
			</form>
		</>
	);
};
