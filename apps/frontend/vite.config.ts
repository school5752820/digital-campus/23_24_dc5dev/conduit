import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react-swc';
import { resolve, dirname } from 'node:path';
import { fileURLToPath } from 'node:url';

export default defineConfig({
	plugins: [react()],
	resolve: {
		alias: {
			'@': resolve(dirname(fileURLToPath(import.meta.url)), './src'),
		},
	},
	server: {
		host: true,
	},
});
