NODE := "docker compose exec node"
PNPM := NODE + " pnpm"

ace *args:
	{{PNPM}} --filter "api" run ace {{args}}

backend *args:
	{{PNPM}} --filter "api" dev {{args}}

frontend *args:
	{{PNPM}} --filter "frontend" dev {{args}}

install *args:
	{{PNPM}} install {{args}}

pnpm *args:
	{{PNPM}} {{args}}
